package com.example.pigeondirectmessenger

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeondirectmessenger.DAOSingleton.DAOChatChannel
import com.example.pigeondirectmessenger.adapter.ChannelListAdapter
import com.example.pigeondirectmessenger.model.ChatChannel
import com.example.pigeondirectmessenger.model.ChatMessage
import java.time.LocalDateTime

class MainActivity : AppCompatActivity() {
    var message = ChatMessage()
    var ch1 = ChatChannel()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_channel_list)
        message.status = "recebido"
        message.message = "Oi"

        ch1.name = "Kayky Nascimento"
        ch1.messages.add(message)
        ch1.lastMessage = message.message

        DAOChatChannel.addChannel(ch1)
        val rvChannels = findViewById<View>(R.id.rvChannelsList) as RecyclerView
        val adapter = ChannelListAdapter(DAOChatChannel.getChannels())

        rvChannels.adapter = adapter
        rvChannels.layoutManager = LinearLayoutManager(this)
    }
}