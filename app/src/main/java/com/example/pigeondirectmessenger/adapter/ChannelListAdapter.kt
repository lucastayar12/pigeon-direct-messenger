package com.example.pigeondirectmessenger.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeondirectmessenger.DAOSingleton.DAOChatChannel
import com.example.pigeondirectmessenger.R
import com.example.pigeondirectmessenger.model.ChatChannel

class ChannelListAdapter(private val listChannel : List<ChatChannel>) : RecyclerView.Adapter<ChannelListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtChannelName: TextView = itemView.findViewById<TextView>(R.id.txtChannelNameLabel)
        val txtLastMessage : TextView= itemView.findViewById<TextView>(R.id.txtLastMessage)
        val txtHour : TextView = itemView.findViewById<TextView>(R.id.txtHour)
        val imgView : ImageView = itemView.findViewById(R.id.imgChannel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val channelView = inflater.inflate(R.layout.chat_channel_item, parent, false)
        return ViewHolder(channelView)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val channel : ChatChannel = listChannel.get(position)
        val txtChannelName = holder.txtChannelName
        txtChannelName.text = channel.name
        val txtLastMessage = holder.txtLastMessage
        txtLastMessage.text = channel.lastMessage
        val txtHour = holder.txtHour
        val message = channel.messages.last()
        txtHour.text = message.hour.toString()
        val imgView = holder.imgView
        imgView.setImageResource(channel.img)
    }

    override fun getItemCount(): Int {
        return DAOChatChannel.getChannels().size
    }


}