package com.example.pigeondirectmessenger.model

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ChatMessage() {
    lateinit var message : String
    @RequiresApi(Build.VERSION_CODES.O)
    var hour = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))
    lateinit var status: String

}