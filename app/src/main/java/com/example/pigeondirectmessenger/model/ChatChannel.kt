package com.example.pigeondirectmessenger.model

import android.widget.ImageView
import com.example.pigeondirectmessenger.R
import kotlin.properties.Delegates

class ChatChannel() {

    lateinit var name : String
    var messages : ArrayList<ChatMessage> = ArrayList()
    lateinit var lastMessage: String
    var img : Int = R.drawable.wow_simbolo_da_horda
}