package com.example.pigeondirectmessenger.DAOSingleton

import com.example.pigeondirectmessenger.model.ChatChannel

object DAOChatChannel {

    private var channels = ArrayList<ChatChannel>()

    fun addChannel(channel: ChatChannel){
        channels.add(channel)
    }

    fun getChannels() : ArrayList<ChatChannel>{
        return channels
    }
}